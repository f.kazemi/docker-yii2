FROM php:7.4-fpm-alpine

RUN apk add --no-cache \
    --virtual .build-dependencies \
    $PHPIZE_DEPS \
    autoconf

# Install dependencies
RUN apk --no-cache --update add \
    libxml2-dev \
    curl-dev \
    libpng-dev \
    libjpeg-turbo-dev \
    libmcrypt-dev \
    imagemagick-dev \
    oniguruma-dev \
    libzip-dev \
    zlib-dev \
    libwebp-dev \
    freetype-dev && \
    rm -rf /tmp/* && \
    rm -rf /var/cache/apk/*

# Configure PHP extensions
RUN docker-php-ext-configure json && \
    docker-php-ext-configure session && \
    docker-php-ext-configure ctype && \
    docker-php-ext-configure tokenizer && \
    docker-php-ext-configure simplexml && \
    docker-php-ext-configure dom && \
    docker-php-ext-configure mbstring && \
    docker-php-ext-configure zip && \
    docker-php-ext-configure pdo && \
    docker-php-ext-configure pdo_mysql && \
    docker-php-ext-configure curl && \
    docker-php-ext-configure iconv && \
    docker-php-ext-configure xml && \
    docker-php-ext-configure phar && \
    docker-php-ext-configure exif && \
    docker-php-ext-configure gd \
            --with-freetype=/usr/include/ \
            --with-webp=/usr/include/ \
            --with-jpeg=/usr/include/ 

# Build and install PHP extensions
RUN docker-php-ext-install json \
    session \
    ctype \
    tokenizer \
    simplexml \
    dom \
    mbstring \
    zip \
    mysqli \
    pdo \
    pdo_mysql \
    curl \
    iconv \
    xml  \
    phar \
    gd \
    exif && \
    pecl install imagick && \
    docker-php-ext-enable imagick && \
    apk del .build-dependencies
    
# Install Composer.
RUN curl -sS http://getcomposer.org/installer | php && \
    mv composer.phar /usr/local/bin/composer && \
    chmod +x /usr/local/bin/composer
    
EXPOSE 9000

CMD ["php-fpm"]
